//
//  PaymentPreamble.swift
//  AlphyPay
//
//  Created by Alvin Jiang on 05/01/2021.
//

import Foundation

public struct PaymentPreamble: Codable {
    
    /* =================================================================
     *                   MARK: - Local Initialization
     * ================================================================= */
    public var accessToken = String()
    public var hash = String()
    public var invoiceNumber = String()
    public var merchantID = Int()
    public var paymentAmount = Double()
    public var paymentDescription = String()
    public var redirectURL = String()
    public var callbackURL = String()
    
    public var buyerEmail = String()
    public var buyerName = String()
    public var buyerPhone = String()
    public var deliveryProvider = String()
    public var locationID = String()
    public var locationName = String()
    public var miscCharges = Double()
    
    public var isPaymentSuccess = false
    public var isMobilePayment = true
    
    enum CodingKeys: String, CodingKey {
        case hash
        case invoiceNumber
        case paymentAmount
        case paymentDescription
        case buyerEmail
        case buyerName
        case buyerPhone
        case deliveryProvider
        case locationName
        case miscCharges
        case isMobilePayment
        case callbackURL = "callbackUrL"
        case redirectURL = "redirectUrl"
        case isPaymentSuccess = "paymentSuccess"
        case locationID = "locationId"
        case merchantID = "merchantId"
    }
    
    /* =================================================================
     *                   MARK: - Class Functions
     * ================================================================= */
    public init(token: String,
                hash: String,
                buyerEmail: String,
                invoiceNumber: String,
                merchantID: Int,
                paymentAmount: Double,
                paymentDescription: String,
                callbackURL: String,
                redirectURL: String) {
        self.accessToken = token
        self.hash = hash
        self.buyerEmail = buyerEmail
        self.invoiceNumber = invoiceNumber
        self.merchantID = merchantID
        self.paymentAmount = paymentAmount
        self.paymentDescription = paymentDescription
        self.callbackURL = callbackURL
        self.redirectURL = redirectURL
    }
}
