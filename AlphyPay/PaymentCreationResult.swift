//
//  PaymentCreationResult.swift
//  AlphyPay
//
//  Created by Alvin Jiang on 05/01/2021.
//

import Foundation

public struct PaymentCreationResult: Codable {
    
    /* =================================================================
     *                   MARK: - Local Initialization
     * ================================================================= */
    public var ok = false
    public var status = String()
    public var warning: String?
    public var error: String?
    public var data: PaymentCreationData?
    public var timestamp = Int()
}

public struct PaymentCreationData: Codable {
    
    /* =================================================================
     *                   MARK: - Local Initialization
     * ================================================================= */
    public var callbackURL = String()
    
    enum CodingKeys: String, CodingKey {
        case callbackURL = "callbackUrl"
    }
}
