//
//  PaymentPreambleResult.swift
//  AlphyPay
//
//  Created by Alvin Jiang on 05/01/2021.
//

import Foundation

public protocol AlphyPayResult {
    func onPaymentSuccess()
    func onPaymentFailed(errorMessage: String)
}
