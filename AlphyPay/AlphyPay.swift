//
//  AlphyPay.swift
//  AlphyPay
//
//  Created by Alvin Jiang on 05/01/2021.
//

import Foundation
import UIKit
import WebKit

public class AlphyPay {
 
    /* =================================================================
     *                   MARK: - Local Initialization
     * ================================================================= */
    private var environment: Env = .development
    private var presentingViewController = UIViewController()
    private var accessToken = String()
    private var delegate: AlphyPayResult?
    
    /* =================================================================
     *                   MARK: - Class Functions
     * ================================================================= */
    public init(_ viewController: UIViewController, env: Env, delegate: AlphyPayResult) {
        self.environment = env
        self.presentingViewController = viewController
        self.delegate = delegate
    }
    
    public func createPayment(paymentPreamble: PaymentPreamble) {
        self.accessToken = paymentPreamble.accessToken
        let body = paymentPreamble
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let JSONData = try? encoder.encode(body)
        if let data = JSONData {
//            print(String(data: data, encoding: .utf8)!)
            
            self.callPaymentAPI(data) { (isSuccess, message) in
                if isSuccess {
                    DispatchQueue.main.async {
                        guard let redirectURL = message else {
                            self.showAlertMessage("Cannot retrieve redirect URL")
                            return
                        }
                        let webController = WebController()
                        webController.redirectURL = redirectURL
                        webController.onCloseCompletionHandler = self.completePayment
                        self.presentingViewController.present(webController, animated: true, completion: nil)
                    }
                }
                else {
                    if let errorMessage = message {
                        self.delegate?.onPaymentFailed(errorMessage: errorMessage)
                    }
                }
            }
        }
        else {
            print("Cannot generate JSON from the object")
            return
        }
    }
    
    private func callPaymentAPI(_ data: Data, completion: @escaping (Bool, String?) -> Void) {
        guard let paymentURL = URL(string: self.environment.paymentURL) else {
            return
        }
        var request = URLRequest(url: paymentURL)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(self.accessToken)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.httpBody = data
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            if let error = error {
                completion(false, error.localizedDescription)
            }
            if let responseData = data {
//                print(String(data: responseData, encoding: .utf8)!)
                
                let decoder = JSONDecoder()
                do {
                    let responseObject = try decoder.decode(PaymentCreationResult.self, from: responseData)
                    
                    if let httpResponse = response as? HTTPURLResponse {
                        if !(200...299).contains(httpResponse.statusCode) {
                            if let errorMessage = responseObject.error {
                                completion(false, errorMessage)
                            }
                            else if let warningMessage = responseObject.warning {
                                completion(false, warningMessage)
                            }
                        }
                        else {
                            if let paymentData = responseObject.data {
                                completion(true, paymentData.callbackURL)
                            }
                            else {
                                completion(false, "Cannot get any data returned")
                            }
                        }
                    }
                }
                catch let error {
                    completion(false, error.localizedDescription)
                }
            }
            else {
                completion(false, "There is no data returned")
            }
        })
        task.resume()
    }
    
    private func completePayment(_ isSuccess: Bool) {
        if isSuccess {
            self.delegate?.onPaymentSuccess()
        }
        else {
            self.delegate?.onPaymentFailed(errorMessage: "Payment cannot be completed")
        }
    }
    
    private func showAlertMessage(_ message: String) {
        let alert = UIAlertController(title: "Message", message: "There is an error encountered: \(message). Please contact customer service", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: nil))
        
        self.presentingViewController.present(alert, animated: true, completion: nil)
    }
}
