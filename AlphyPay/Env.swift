//
//  Env.swift
//  AlphyPay
//
//  Created by Alvin Jiang on 05/01/2021.
//

import Foundation

public enum Env {
    case development
    case staging
    case production
    
    var gatewayURL: String {
        switch self {
        case .development:
            return "https://alphypay-api-staging.azurewebsites.net"
        case .staging:
            return "https://alphypay-api-staging.azurewebsites.net"
        case .production:
            return "https://api.alphypay.com"
        }
    }
    
    var redirectURL: String {
        return "\(self.gatewayURL)/extPay/receipt/exit"
    }
    
    var paymentURL: String {
        return "\(self.gatewayURL)/Payment/pay"
    }
}
