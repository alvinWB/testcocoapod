//
//  WebController.swift
//  AlphyPay
//
//  Created by Alvin Jiang on 06/01/2021.
//

import UIKit
import WebKit

public class WebController: UIViewController {

    /* =================================================================
     *                   MARK: - Local Initialization
     * ================================================================= */
    var redirectURL = String()
    var onCloseCompletionHandler: ((Bool) -> Void)?
    private var webView: WKWebView!
    private var navigationBar = UINavigationBar()
    private let progressView: UIProgressView = {
        let progressView = UIProgressView(progressViewStyle: .bar)
        progressView.sizeToFit()
        progressView.progressTintColor = UIColor(red: 255/255, green: 204/255, blue: 0/255, alpha: 1.0)
        
        return progressView
    }()
    
    /* =================================================================
     *                   MARK: - Class Functions
     * ================================================================= */
    public override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        self.setupNavigationBar()
        
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true
        }
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setupWebView()
        self.setupProgressBar()
    }
    
    @objc private func dismissWebView() {
        if let webViewURL = self.webView.url {
            if !webViewURL.absoluteString.contains("?transactionStatus=") {
                self.showAlertMessage()
            }
            else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    private func showAlertMessage() {
        let alert = UIAlertController(title: "Message", message: "Transaction is not completed. Proceed to dismiss?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Proceed", style: .default, handler: { action in
            self.dismiss(animated: true) {
            self.onCloseCompletionHandler?(false)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setupNavigationBar() {
        self.navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44))
        let rightButton = UIBarButtonItem(title: "Close", style: UIBarButtonItem.Style.plain, target: self, action: #selector(dismissWebView))
        let customNavigationItem = UINavigationItem(title: "Alphy Pay")
        customNavigationItem.title = "Alphy Pay"
        customNavigationItem.rightBarButtonItem = rightButton
        
        self.navigationBar.setItems([customNavigationItem], animated: true)
        self.navigationBar.barTintColor = .white
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.navigationBar.isTranslucent = true
        
        if #available(iOS 11.0, *) {
            let top = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
            self.navigationBar.frame = CGRect(x: 0, y: top, width: self.view.frame.size.width, height: 50)
        }
        self.view.addSubview(self.navigationBar)
    }
    
    private func setupWebView() {
        let webConfiguration = WKWebViewConfiguration()
        let customFrame = CGRect(x: 0, y: self.navigationBar.frame.maxY, width: self.view.frame.size.width, height: self.view.frame.size.height - (self.navigationBar.frame.maxY + 44))
        self.webView = WKWebView(frame: customFrame, configuration: webConfiguration)
        self.webView.navigationDelegate = self
        
        let web = URL(string: self.redirectURL)!
        self.webView.load(URLRequest(url: web))
        self.webView.allowsBackForwardNavigationGestures = true
        self.webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        
        self.view.addSubview(self.webView)
    }
    
    private func setupProgressBar() {
        self.progressView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 20)
        self.webView.addSubview(progressView)
    }
    
    private func getParameterValue(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value?.uppercased()
    }
    
    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            self.progressView.progress = Float(webView.estimatedProgress)
            if self.progressView.progress == 1.0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.progressView.isHidden = true
                }
            }
        }
    }
}

/* =================================================================
 *               MARK: - WKNavigation Delegate
 * ================================================================= */
extension WebController: WKNavigationDelegate {
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if let newURL = webView.url {
//            print(newURL)
            if newURL.absoluteString.contains("?transactionStatus=") {
                if let transactionStatus = self.getParameterValue(url: newURL.absoluteString, param: Receipt.transactionStatus.rawValue) {
                    if transactionStatus == TransactionStatus.success.rawValue.uppercased() {
                        self.dismiss(animated: true, completion: nil)
                        self.onCloseCompletionHandler?(true)
                    }
                    else {
                        self.dismiss(animated: true, completion: nil)
                        self.onCloseCompletionHandler?(false)
                    }
                }
            }
        }
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(WKNavigationActionPolicy(rawValue: WKNavigationActionPolicy.allow.rawValue + 2)!)
    }
}
