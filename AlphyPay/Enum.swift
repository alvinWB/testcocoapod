//
//  Enum.swift
//  AlphyPay
//
//  Created by Alvin Jiang on 07/01/2021.
//

import Foundation

enum Receipt: String {
    case transactionStatus = "transactionStatus"
    case transactionDetails = "transactionDetails"
}

enum TransactionStatus: String {
    case success = "00"
}

enum TransactionDetails: String {
    case approved = "Approved"
    case success = "Success"
}
