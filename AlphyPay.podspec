
Pod::Spec.new do |spec|

  spec.platform = :ios
  spec.swift_version = "5.0"
  spec.ios.deployment_target = "12.0"
  spec.name = "AlphyPay"
  spec.summary = "AlphyPay SDK"
  spec.requires_arc = true
  spec.version = "1.2"
  spec.license = { :type => "MIT", :file => "LICENSE" }
  spec.homepage = "https://gitlab.com/alvinWB/testcocoapod"
  spec.source = { :git => "https://gitlab.com/alvinWB/testcocoapod.git",
                  :tag => "#{spec.version}" }
  spec.frameworks = 'Foundation', 'UIKit', 'WebKit'
  spec.author = { "alvin" => "alvin@webbytes.com.my" }
  spec.source_files = "AlphyPay/*.swift"

end
